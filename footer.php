<?php

?>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script>
    	$(function () {
			$('[data-toggle="tooltip"]').tooltip();

            $('body').scrollspy({ target: '#scrollspy-wrap', offset: 100 });
		})
    </script>
    <script>
        $(window).scroll(function () {
            
            //30 is the extra padding at the bottom of the #featured element
            var fixedHeight = document.getElementById('featured').offsetHeight+30;
            var navWidth = document.getElementById('scrollspy-nav').offsetWidth;

            if ($(window).scrollTop() > fixedHeight) {
                $('#scrollspy-nav').addClass('nav-fixed');
                document.getElementById("scrollspy-nav").style.width = navWidth+"px";
            }
            if ($(window).scrollTop() < fixedHeight) {
                $('#scrollspy-nav').removeClass('nav-fixed');
            }            
        }); 

        $(function() {
            $('a[href*=#]:not([href=#])').click(function() {
                
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                
                    var target = $(this.hash);
                    
                    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                    
                    var scrollTo = target.offset().top-63;

                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: scrollTo
                        }, 1000);
                        return false;
                        }
                    }
                });
            });

    </script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
      
  </body>
</html>
