<?php include('header.php'); ?>

    <!-- Fixed navbar -->
    <?php include("includes/nav/nav-application.php"); ?>

    <!-- Start of Container -->
    <div class="container theme-showcase application" role="main">

        <div class="col-md-12">
            <?php include("includes/upwork-application/jumbotron.php");?>
        </div>

        <div class="col-md-9">
            <?php 
                //Block of the Upwork Application
                include("includes/upwork-application/upwork-responses.php"); 
            ?>
            <hr/>
    		
    		<?php
                //Block of the Upwork Screening Questions 
                include("includes/upwork-application/upwork-screening.php"); 
            ?>
            <br />
            <div class="jumbotron">
                <center>
            		<h3 id="learn-more">Let's learn more</h3>
                    <a href="about.php" type="button" class="btn btn-lg btn-info"><strong>About Christian</strong></a></h3>
                </center>
                <br/>
            </div>

    	</div>

    	<div class="col-md-3 app-nav-main">
    		<?php
                //Block of the side nav for application 
                include("includes/upwork-application/application-nav.php"); 
            ?>
    	</div>

    </div> <!-- end of container -->

<?php include('footer.php'); ?>