<?php include('header.php'); ?>

    <!-- Fixed navbar -->
    <?php include("includes/nav/nav-twig.php"); ?>

    <!-- Start of Container -->
    <div class="container theme-showcase twig" role="main">

      <div class="col-lg-12">

        <?php
          //Jumbotron block of twig.php
          include("includes/twig/jumbotron.php"); 
        ?>

          <button type="button" class="btn btn-block btn-lg btn-warning" style="font-size:28px;font-weight:bold;">Page Under Construction <span class="glyphicon-class glyphicon glyphicon-wrench" style="font-size:26px;"></span></button>

      </div>

      <div class="col-md-12">

        <?php 
          //Contact Details Block of about.php
          include("includes/twig/main.php");
        ?>
        
        <hr/>

      </div>

    </div> <!-- End of Container -->

<?php include('footer.php'); ?>
