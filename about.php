<?php include('header.php'); ?>

    <!-- Fixed navbar -->
    <?php include("includes/nav/nav-about.php"); ?>

    <!-- Start of Container -->
    <div class="container theme-showcase about" role="main">

      <div class="col-lg-12">
        <?php
          //Jumbotron block of about.php
          include("includes/about/jumbotron.php"); 
        ?>
      </div>

      <div class="col-md-9">

        <?php 
          //Contact Details Block of about.php
          include("includes/about/contact-details.php");

          //Skills Block of about.php
          include("includes/about/skills.php");

          //Education Block of about.php
          include('includes/about/education.php'); 

          //Past Experience Block of about.php
          include('includes/about/past-experience.php');

          //Coming Soon Block of about.php
          include('includes/about/coming-soon.php');
        ?>
        
        <hr/>

      </div>

    	<div class="col-md-3 about-nav-main">
        <?php include("includes/about/about-nav.php"); ?>
    	</div>

    </div> <!-- End of Container -->

<?php include('footer.php'); ?>
