<?php 
	/* Navigation Menu
	*
	*/
?>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Christian Rios</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="application.php"><span class="fui-document"></span> Hero Bureau Application</a></li>
            <li><a href="about.php"><span class="fui-user"></span> About Christian</a></li>
            <li><a href="twig.php"><span class="fui-window"></span> Next Task: Twig</a></li>
            <li><a href="https://bitbucket.org/christianrios/" target="_blank"><span class="fui-window"></span> Git</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

