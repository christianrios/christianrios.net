<?php 
	/*
	* 	Education Block of resume.php
	*/
?>
    <div class="jumbotron">
		<h3 id="education">Education</h3>
		<p>
			<img src="img/psu.jpg" style="float:left;max-width:100px;height:auto;" />
			<strong>Bachelor of Science in Information Science and Technology</strong><br/>
			Pennsylvania State University<br/>
			May 2008<br/>
		</p>
      
		<blockquote>
        	<p><em>
        		The Information Sciences and Technology (BS) degree program helps organizations achieve strategic goals by bridging technology solutions with user requirements. This includes skill sets in project management, programming, entrepreneirial thinking, social understanding and application development.
        	</em></p>
		</blockquote>
	</div>