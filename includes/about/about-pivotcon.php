			<div class="jumbotron">
	      		
	      		<div class="modal-header">
 	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      			<h4 class="modal-title" id="myModalLabel">Engagement Projects with Sponsors</h4>
 	        	</div>

 				<div class="modal-body">
	              	<p>We worked on some cool initiatives to increase the user engagement on <a href="//www.pivotcon.com"><strong>Pivotcon.com</strong></a>. This included: 
	                	<ul>
		                  	<li>Gamifying a site using <a href="//www.badgeville.com" data-toggle="tooltip" data-placement="bottom" title="Badgeville: The Leading Business Gamification Company"><strong>Badgeville</strong></a></li> 
		                  	<li>Generating discount codes based on twitter influence using <a href="//www.klout.com" data-toggle="tooltip" data-placement="top" title="The Klout Score is a number between 1-100 that represents your influence. The more influential you are, the higher your Klout Score"><strong>Klout</strong></a> and <a href="//www.eventbrite.com" data-toggle="tooltip" data-placement="top" title="Eventbrite is the world's largest self-service ticketing platform" ><strong>Eventbrite</strong></a></li>
		                  	<li>Building internal research tools (aka API Mash-ups) using <a href="//www.kred.com" data-toggle="tooltip" data-placement="bottom" title="Measure your Influence with a Kred Score"><strong>Kred</strong></a></li>
		                	<li>Customized social-login experience with <a href="//www.janrain.com" data-toggle="tooltip" data-placement="bottom" title="Janrain specializes in customer identity data management and social login systems so businesses know their customers and leverage data to deliver ..."><strong>Janrain</strong></a></li>
		                </ul>	                
	            	</p>
	            </div>

				<div class="modal-footer">
				<center>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</center>
				</div>
            </div>
