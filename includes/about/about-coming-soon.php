

			<!-- Start of Jumbotron -->
			<div class="jumbotron">
	      		
	      		<div class="modal-header">
 	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      			<h4 class="smodal-title" id="myModalLabel">"But this is so short!?"</h4>
 	        	</div>

 				<div class="modal-body">
	              	<p>You're right, it is... for now!</p>
	              	<p>I currently have one other project I'm getting ready to hand-off, but I wanted to make sure this site was launched to assist with the interview process with Hero Bureau.  I wanted to touch on a little bit of everything.</p> 
	              	<p>I also expect to have this site powered by Twig Templates before<br/>
	              	<center>
	              		<button type="button" class="btn btn-s btn-inverse"><strong>November 5th, 2015</strong></button><br/></p>
	              	</center>
	              	<p>In the meantime, you can check out my <a href="https://www.upwork.com/o/profiles/users/_~01a2b698951fa0944a/"><strong>Upwork Profile</strong></a> for some more portfolio examples.</p>
	              	<p><caption><em>Note: I would consider the upwork examples dated, but would still love to talk about them :)</em></caption></p>
	            </div>

				<div class="modal-footer">
					<center>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</center>
				</div>

            </div>
            <!-- End of Jumbotron -->
