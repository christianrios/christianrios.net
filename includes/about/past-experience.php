  <div class="jumbotron">
    <h3 id="past-experience">Past Experience</h3>
    <ul class="past-experience">
      <li>
        <ul>
          <li>
            <p>
              <strong>The Tomorrow Project</strong> | New York, NY<br/>
              WordPress Developer<br/>
              2010-2013
            </p>
            <p>
              My primary role with The Tomorrow Project was to help develop and maintain multiple WordPress sites.</p>
            <p>
              Projects:
              <ul>
                <li>Maintaining the "old" WordPress website <a class="btn btn-xs btn-info" href="https://web.archive.org/web/20130315175123/http://2012.pivotcon.com/" target="_blank"><strong>"Old" Site</strong></a></li>
                <li>Lead the development for the "new" website, built using Foundation by ZURB <a class="btn btn-xs btn-info" href="https://web.archive.org/web/20131210160423/http://pivotcon.com/" target="_blank"><strong>"New" Site</strong></a></li>
                <li>Led communication and integration efforts with Partnering Sponsors <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#myModal"><strong>Learn More</strong></button></li>                  
              </ul>
            </p>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">

                <?php include('about-pivotcon.php'); ?>

              </div>
            </div>
          
          </li>
        </ul>
      </li>
    </ul>
  
  </div>