    <div class="jumbotron"> 
      <h3 id="contact-details">Contact Details</h3>
      
      <table class="table table-bordered">
        <tbody>
        
          <tr>
            <th scope="row">Name</th>
            <td>Christian Rios</td>
          </tr>
        
          <tr>
            <th scope="row"><span class="fui-mail"></span></th>
            <td>crios121@gmail.com</td>
          </tr>
        
          <tr>
            <th scope="row"><span class="fui-skype"></span></th>
            <td>crios121</td>
          </tr>
        
        </tbody>
      </table>
</div>
