          
      <div class="jumbotron">
      	    <center>
	      		<h3 id="coming-soon"><span class="fui-info-circle" style="font-size:30px;"></span> More Coming Soon!!</h3>
	      		<br/>
            <button type="button" class="btn btn-block btn-md btn-danger" data-toggle="modal" data-target="#myModal2">Wait, this is the end?</button>
            <br/>
      		</center>
      </div>
      <!-- Start of Modal -->
      <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">

            <?php include("about-coming-soon.php"); ?>

        </div>
      </div>
      <!-- End of Modal -->