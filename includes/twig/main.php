<ul class="timeline">
    <li>
		<div class="timeline-badge"><i class="glyphicon glyphicon-check"></i></div>
		<div class="timeline-panel">
			<div class="timeline-heading">
				<center>
          			<h4 class="timeline-title">1) Learn More About Twig</h4>
          		</center>
        	</div>
	        <div class="timeline-body">
		        <p>With the help of the official Twig documentation and youtube tutorials, I will learn the basics of Twig and arm myself with enough knowledge to plan out how to Twig-ify this site.</p>
	        </div>
		</div>
    </li>
    <li class="timeline-inverted">
		<div class="timeline-badge"><i class="glyphicon glyphicon-check"></i></div>
		<div class="timeline-panel">
        	<div class="timeline-heading">
        		<center>
					<h4 class="timeline-title">2) Plan Out the Great Twig Conversion</h4>
        		</center>
        	</div>
	        <div class="timeline-body">
	        	<p>Main goal will be to leverage the embed tag, as well as basic logic, and the ability to pass JSON data into the Twig templates.  This should greatly enhance future site-mainetance and updates.</p>
	        </div>
		</div>
    </li>
    <li>
		<div class="timeline-badge"><i class="glyphicon glyphicon-check"></i></div>
		<div class="timeline-panel">
			<div class="timeline-heading">
				<center>
          			<h4 class="timeline-title">3) Hack Away</h4>
        		</center>
        	</div>
	        <div class="timeline-body">
	        	<center>
		        	<i class="glyphicon glyphicon-blackboard" style="font-size:90px;"></i>
		    	</center>
	        </div>
		</div>
    </li>
    <li class="timeline-inverted">
		<div class="timeline-badge"><i class="glyphicon glyphicon-check"></i></div>
		<div class="timeline-panel">
        	<div class="timeline-heading">
        		<center>
					<h4 class="timeline-title">4) Re-Launch the Site</h4>
        		</center>
        	</div>
	        <div class="timeline-body">
	        	<p>Reintroduce this site into the wild and begin expanding some more, now using Twig Templates :)</p>
	        </div>
		</div>
    </li>
</ul>