<div class="jumbotron">

	<h3>Screening Questions</h3>

	<!-- Question 1-->
	<?php include('screening-questions/question-01.php');?>
	<!-- End of Question 1-->

	<!-- Question 2-->
	<?php include('screening-questions/question-02.php');?>
	<!-- End of Question 2-->

	<!-- Question 3-->
	<?php include('screening-questions/question-03.php');?>
	<!-- End of Question 3-->

	<!-- Question 4-->
	<?php include('screening-questions/question-04.php');?>
	<!-- End of Question 4-->

	<!-- Question 5-->
	<?php include('screening-questions/question-05.php');?>
	<!-- End of Question 5-->

</div>