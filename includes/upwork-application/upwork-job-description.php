<?php
	//
	//
	// The Original Upwork Job Description
	//
	//
?>
		<!-- Modal -->
	    <div class="modal-content">
	      
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Job Description</h4>
	      </div>

			<div class="modal-body">
				<ul>
					<li>Looking for a front end developer with experience in Bootstrap, CSS, HTML, JS, PHP, Github and responsive mobile design for an ongoing role.</li>
					<li>Experience in Twig Templates would be a plus but is not essential.
					<li>Experience with Jira project management would be helpful but is also not essential.</li>
					<li>You should have experience constructing wireframes.</li>
					<li>This is a FULL TIME role working 8 hours per day 5 days per week. Of your 8 hour day you must be able to work at least 2 US hours per day (Mountain - MST Time Zone). </li>
					<li>Our team works in the agile methodology with daily meetings at 8am MST. You must be able to attend these meetings.</li>
					<li>The role commences immediately.</li>
					<li>Please answer the Screening Questions properly.</li>
					<li>The role involves:
						<ul>
							<li>Provide ongoing support working with a back-end technical team and a project manager.</li>
						</ul>
					</li>
					<li>In general when we provide you with a task, we are looking for you to do the following:
						<ul>
							<li>(i) adjust bootstrap, css, php, JS, and twig templates to meet exact design specifications</li>
							<li>(*) Use image editing software to tweak images</li>
							<li>(*) Use Ajax to tune page responsiveness</li>
							<li>(ii) Use comments to document work </li>
							<li>(iii) test your work on local development (virtual box and Vagrant, etc., or other local web server)</li>
							<li>(*) commit work to Git repository (Git proficiency REQUIRED)</li>
							<li>(iv) submit a daily email summary of screeshots, work and tests completed</li>
						</ul>
					</li>
				<ul>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>

	    </div>
