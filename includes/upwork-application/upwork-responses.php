<div class="jumbotron">
    <h3 id="my-application">My Application</h3>
    <hr/>
	<h4 id="required-skills">Required Skills</h4>

	<p>Of all the <a <?php /*button class="btn btn-l btn-inverse" */ ?> href="#" data-toggle="tooltip" data-placement="bottom" title="Bootstrap, CSS, HTML, JS, PHP, Github, Responsive Mobile Design, Twig Templates, and Jira">technologies you listed</a <?php /* button */ ?>>, I have experience in all of the following, EXCEPT:</p>
	<ul>
		<li>Twig Templates</li>
		<li>Jira Proejct Management</li>
	</ul>

	<p>I do have vast experience with WordPress... while it doesn't replace Twig templates 100%, it can follow a very similar process and I happen to use a similar flow when coding Custom WordPress theme's for my clients (generic php includes).</p>

	<p>With regards to Jira, I have used Basecamp in the past, so I think I can pick up on it quick.</p>

	<h4 id="wireframe-experience">Experience Constructing Wireframes</h4>

	<p>In all of my builds, I start out building wireframes and prototypes (basic HTML/CSS/JS).</p>  
	<p>I learned this workflow from ZURB (a distinguished design company in Seattle).</p>

	<ul>
		<li><a href="http://zurb.net/zurbwired2011/projects/zurbwired2011/frame/prototype/public/" target="_blank">Foundation Prototype Example 1</a></li>
		<li><a href="http://foundation.zurb.com/templates/feed.html" target="_blank">Foundation Prototype Example 2</a></li>
	</ul> 

	<h4 id="availability">Availability</h4>
	<p>I'm currently in Colombia, and after this Sunday (Daylight Savings Ends) I will have a +2 hour time difference from the MST.  So 8AM meetings for me will be 10AM local, which is no issue whatsoever. </p>
	<p>I'm currently working on two projects that end in a few weeks, at which point, if accepted, I can devote my full attention to your team.</p>

	<p>----</p>

	<p>Please let me know if you'd like to discuss the position further and if you have any other questions/comments for me.</p>

	<h4>Regards,<br/>
	Christian</h4>
</div>