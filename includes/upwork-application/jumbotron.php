	<div id="featured" class="main jumbotron">
        <h1>
        	Hero Bureau Application
			    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">
				   Review Job Description
			    </button>
        </h1>

		<p><caption>As originally submitted by Christian, but be on the lookout for <button type="button" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="bottom" title="In which case, it'll be added at the end"><strong>Click for Update</strong></button> buttons</caption></p>  	
  	</div>

    <!-- Button trigger modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">

			<?php include('upwork-job-description.php'); ?>

		</div>
	</div>