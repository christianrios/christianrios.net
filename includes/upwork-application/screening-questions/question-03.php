<h4 id="question-3">3) How long have you worked with Twig Templates? Examples?</h4>
	<p>Not Available</p>

	<center>
		<span data-toggle="modal" data-target="#twig-templates">
			<button type="button" class="btn btn-block btn-lg btn-info update" data-toggle="tooltip" data-placement="top" title="Time to Twig-ify this site"><strong>Update - Read about my most recent Twig experience</strong></button>
		</span>
	</center>	

	<!-- Modal Start-->
	<div class="modal fade" id="twig-templates" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Twig Templates</h4>
	      </div>
	      
	      <div class="modal-body">
	        <p>This site was first built using Bootstrap and basic/core html, css, and js files.  After the initial launch, I am determined to go through all the code again and re-code using Twig Templates.</p>
	      </div>
	      
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>

	    </div>
	  </div>
	</div>
	<!-- Modal End-->
	<hr/>