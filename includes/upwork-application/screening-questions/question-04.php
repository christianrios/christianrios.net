	<h4 id="question-4">4) Are you familiar with tools like Github and/or Jira? How have you used them in the past?</h4>
	<p>I have a personal Git Repo on my Linux machine and have not used Jira, but Basecamp in the past.</p>

	<center>
		<span data-toggle="modal" data-target="#git-jira">
			<button type="button" class="btn btn-block btn-lg btn-info update" data-toggle="tooltip" data-placement="top" title="This site's git repo should explain how I use git in my projects"><strong>Update - Git and Jira Experience</strong></button>
		</span>
	</center>	

	<!-- Modal Start-->
	<div class="modal fade" id="git-jira" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Git and Jira Experience</h4>
	      </div>
	      
	      <div class="modal-body">
	        <p>Check out this site's <a href="https://bitbucket.org/christianrios/christianrios.net/" class="btn btn-s btn-inverse">git repo</a> if you haven't already yet</p>
	        <p>With regards to Jira, I haven't used it in the past.  I've used Basecamp for previous projects</p>
	        <p>After reading up more on Jira, I realize it is a project management tool focused around the agile software development methodology and looks <strong>very similar</strong> to <em>Trello</em>, "the free, flexible, and visual way to organize anything with anyone."</p>
	      </div>
	      
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>

	    </div>
	  </div>
	</div>
	<!-- Modal End-->
	<hr/>