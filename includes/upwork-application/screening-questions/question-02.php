<h4 id="question-2">2) How long have you worked with CSS, HTML, PHP and JS? Examples? Please SUBMIT A CODE SAMPLE in your application for us to review.</h4>

	<p>All of the theme's I've worked with in WordPress rae built using CSS/HTML/PHP.</p>
	<p>Unfortunately, I don't have access to servers to paste those specific code sampes - embarassing to note have something like that ready for you :/</p>
	<p>But I can share sites to see some source codes if you'd like</p>
	<ul>
		<li><a href="//pivotcon.com">www.pivotcon.com</a></li>
		<li><a href="//cultureworksphila.org">www.cultureworksphila.org</a></li>
		<li><a href="//culturetrustphila.org">www.culturetrustphila.org</a></li>
		<li><a href="//chathamclub.com">www.chathamclub.com</a></li>
	</ul>
	<p>I can explain the PHP logic in each, which may hopefully suffice.</p>

	<center>
		<span data-toggle="modal" data-target="#code-example">
			<button type="button" class="btn btn-block btn-lg btn-info update" data-toggle="tooltip" data-placement="top" title="Let's git init"><strong>Update - The most recent code I wrote</strong></button>
		</span>
	</center>	

	<!-- Modal Start-->
	<div class="modal fade" id="code-example" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Code Sample</h4>
	      </div>
	      
	      <div class="modal-body">
	      	<h5>ChristianRios.net</h5>
	        <p>You can review this site's code here in this <a class="btn btn-s btn-inverse" href="https://bitbucket.org/christianrios/christianrios.net" target="_blank" alt="">git repo</a> I created specifically for this project.</p>
	        <h5>CataBalzano.com</h5>
	        <p>I am in the process of converting a Wix site to WordPress.  The following is a link to the custom WordPress plugin <a class="btn btn-s btn-inverse" href="https://bitbucket.org/christianrios/cata-wp-plugin/" target="_blank">git repo</a></p>
	      </div>
	      
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>

	    </div>
	  </div>
	</div>
	<!-- Modal End-->

	<hr/>