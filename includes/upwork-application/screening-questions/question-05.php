	<h4 id="question-5">5) (optional) Please submit examples of any wireframes you may have built.</h4>
	
	<p>See response to #1<br/></p>

	<center>
		<span data-toggle="modal" data-target="#wireframe">
			<button type="button" class="btn btn-block btn-lg btn-info update" data-toggle="tooltip" data-placement="top" title="In-browser prototype/wireframe"><strong>Update - Here's a better response</strong></button>
		</span>
	</center>	

	<!-- Modal Start-->
	<div class="modal fade" id="wireframe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Wireframe Example</h4>
	      </div>
	      
	      <div class="modal-body">
	        <p>The following is an example of an in-browser wireframe/prototype built based off of the client's design specs.</p>
	        <button type="button" class="btn btn-s btn-inverse">wireframe example</button>
	      </div>
	      
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>

	    </div>
	  </div>
	</div>
	<!-- Modal End-->

	<hr/>