	<h4 id="question-1">1) How long have you worked with Twitter Bootstrap? Examples?</h4>

	<p>I have worked with Twitter Bootstrap very sparingly, but I am quite familiar with the methodology.  I started a lot of my work with ZURB's Foundation (www.pivotcon.com)</p>
	<p>I'm currently working on a personal project and tinkering right now with a layout on Twitter Bootstrap (<a href="//www.christianrios.net/pntm" target="_blank">www.christianrios.net/pntm</a>).  This is acting as a wireframe/prototype (using <a href="//www.lorempixel.com" target="_blank">www.lorempixel.com</a> as image placeholders).  </p>
	<p><em>NOTE: The skewed look in "The Daily Beast" section is purposeful - I'm working out a bug with varying character lengths.</em></p>

	<center>
		<span data-toggle="modal" data-target="#twitter-bootsrap">
			<button type="button" class="btn btn-block btn-lg btn-info update" data-toggle="tooltip" data-placement="top" title="Guess what this site is built using?"><strong>Update - Read about my latest Twitter Bootstrap experience</strong></button>
		</span>
	</center>	

	<!-- Modal Start-->
	<div class="modal fade" id="twitter-bootsrap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Bootstrap Experience</h4>
	      </div>
	      
	      <div class="modal-body">
	        <p>As I stated in my original response,  I have only tinkered with Bootstrap in the past.</p>
	        <p>With that said, I thought it would be necessary to demonstrate that I can quickly turnaround a [simple] site built on Bootstrap - hence, the existence of this site.</p>
	        <p>So on late Monday, November 2, 2015 - I decided to develop a bootstrap-based site specifically for my <strong>Hero Bureau</strong> application.</p>
	        <p>I launched late Tuesday, November 3, 2015 (or early Wednesday, November 4, 2015)</p>
	      </div>
	      
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>

	    </div>
	  </div>
	</div>
	<!-- Modal End-->

	<hr/>