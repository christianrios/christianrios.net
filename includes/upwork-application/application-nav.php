		<div id="scrollspy-wrap" class="visible-md visible-lg">
			<ul id="scrollspy-nav" class="nav nav-tabs app-nav">
				<li><h5>My Application</h5></li>
				<li><a href="#required-skills">Required Skills</a></li>
				<li><a href="#wireframe-experience">Experience Constructing Wireframes</a></li>
				<li><a href="#availability">Availability</a></li>
				<li><h5>Screening Questions</h5>
				<li><a href="#question-1">How long have you worked with Twitter Bootstrap? Examples?<button type="button" class="btn btn-info btn-xs">Update</button></a></li>
				<li><a href="#question-2">How long have you worked with CSS, HTML, PHP and JS? Examples? Please SUBMIT A CODE SAMPLE in your application for us to review.<button type="button" class="btn btn-info btn-xs">Update</button></a></li>
				<li><a href="#question-3">How long have you worked with Twig Templates? Examples?<button type="button" class="btn btn-info btn-xs">Update</button></a></li>
				<li><a href="#question-4">Are you familiar with tools like Github and/or Jira? How have you used them in the past?<button type="button" class="btn btn-info btn-xs">Update</button></a></li>
				<li><a href="#question-5">(Optional) Please submit examples of any wireframes you may have built. <button type="button" class="btn btn-info btn-xs">Update</button></a></li>
				<li><h5>Learn More</h5></li>
				<li><a href="#learn-more">About Christian</a></li>
			</ul>
		</div>